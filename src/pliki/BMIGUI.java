package pliki;

//********************************************************************
//  BMIGUI.java     
//
//  Wyznacza BMI w GUI.
//********************************************************************

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class BMIGUI
{
   private int WIDTH = 330;
   private int HEIGHT = 140;

   private JFrame frame;
   private JPanel panel;
   private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel;
   private JTextField wzrost, waga;
   private JButton oblicz;

   //-----------------------------------------------------------------
   //  Ustawia GUI.
   //-----------------------------------------------------------------
   public BMIGUI()
   {
      frame = new JFrame ("Kalkulator BMI");
      frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

      //tworzenie etykiety dla pol tekstowych wzrostu i wagi 
      wzrostLabel = new JLabel ("Twoj wzrost w metrach:");
      wagaLabel = new JLabel ("Twoja waga w kilogramach: ");

      //stworz etykiete "to jsest twoje BMI" 
      BMILabel = new JLabel ("Twoje BMI:");
      //stworz etykiete wynik dla wartosci BMI
      wynikLabel = new JLabel ();
      wynikLabel.setPreferredSize(new Dimension(200, 20));
      // stworz JTextField dla wzrostu
      wzrost = new JTextField("", 10);
      // stworz JTextField dla wagi
      waga = new JTextField("", 10);
      // stworz przycisk, ktory po wcisnieciu policzy BMI
      // stworz BMIListener, ktory bedzie nasluchiwal czy przycis zostal nacisniety 
      oblicz = new JButton("Oblicz");
      oblicz.addActionListener(new BMIListener());
      // ustawienia JPanel znajdujacego sie na JFrame 
      panel = new JPanel();
      panel.setPreferredSize (new Dimension(WIDTH, HEIGHT));
      panel.setBackground (Color.yellow);

      //dodaj do panelu etykiete i pole tekstowe dla wzrostu
      //dodaj do panelu etykiete i pole tekstowe dla wagi
      //dodaj do panelu przycisk
      //dodaj do panelu etykiete BMI
      //dodaj do panelu etykiete dla wyniku

      //dodaj panel do frame
      panel.add(wzrostLabel);
      panel.add(wzrost);
      panel.add(wagaLabel);
      panel.add(waga);
      
      panel.add(BMILabel);
      panel.add(wynikLabel);
      panel.add(oblicz);
      
      
      frame.getContentPane().add (panel);
      
   }

   //-----------------------------------------------------------------
   //  Wyswietl frame aplikacji podstawowej
   //-----------------------------------------------------------------
   public void display()
   {
      frame.pack();
      frame.show();
   }

   //*****************************************************************
   //  Reprezentuje action listenera dla przycisku oblicz.
   //*****************************************************************
   private class BMIListener implements ActionListener
   {
      //--------------------------------------------------------------
      //  Wyznacz BMI po wcisnieciu przycisku
      //--------------------------------------------------------------
      public void actionPerformed (ActionEvent event)
      {
         String wzrostText = wzrost.getText(), wagaText = waga.getText();
         double wzrostVal = Double.parseDouble(wzrostText);
         int wagaVal = Integer.parseInt(wagaText);
         double bmi = wagaVal/(wzrostVal*wzrostVal);
         wynikLabel.setText(Double.toString(bmi));
	 //pobierz tekst z pol tekstowych dla wagi i wzrostu

	 //Wykorzystaj Integer.parseInt do konwersji tekstu na integer

	 //oblicz  bmi = waga / (wzrost)^2

	 //Dodaj wynik do etykiety dla wyniku. 
	 // Wykorzystaj Double.toString do konwersji double na string.

      }
   }
}

